module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    grunt.registerTask('build', [
        'clean:dist',
        'wiredep',
        'ngconstant:dist',
        'useminPrepare',
        'concurrent:dist',
        'babel:es6',
        'autoprefixer',
        'concat',
        'ng-annotate',
        'copy:dist',
        'cdnify',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin'
    ]);
    grunt.initConfig({
        watch: {
            babel: {
                files: ['app/scripts/**/*.es6'],
                tasks: ['babel']
            }
        },


        babel: {
            es6: {
                files: [{
                    expand: true,
                    src: ['app/scripts/**/*.es6'],
                    ext: '.js'
                }]
            }
        },
        babel: {
            es6: {
                options: {
                    blacklist: ['strict']
                },
                files: [{
                    expand: true,
                    src: ['app/scripts/**/*.es6'],
                    ext: '.js'
                }]
            }
        }
    });
}